#!/usr/bin/env python
import requests

class HueInterface:
    def __init__(self, bridge_ip):
        self.name = "shutter_lighting"

        self.ip = bridge_ip
        self.base_url = "https://" + self.ip + "/api"

        self.user = None

    def create_user(self):
        # press bridge link button before call
        req = {"devicetype" : "shutter_lighting#hue_control"}
        response = requests.post(self.base_url, json=req)

        if response:
            print("Successfully created new user")
            self.user = response.json()["success"]["username"]
        else:
            raise ApiError("POST /api new_device {} {}".format(response.status_code, response.json()["error"]["description"]))

    def get_lights(self):
        if not self.user:
            raise ApiError("New user not registered")

        url = self.base_url + self.user + "/lights"
        response = requests.get(url)

        return response.json()

    def get_light_state(self, light_id):
        url = self.base_url + self.user + "/lights" + str(light_id)+ "/state"
        response = requests.get(url)

    def put_brightness(self, light_id, brightness):
        req = {"on": True, "sat": 254, "bri": brightness, "hue":10000}
        url = self.base_url + self.user + "/lights" + str(light_id)+ "/state"
        response = requests.put(url, data=req)

        return response.status_code
