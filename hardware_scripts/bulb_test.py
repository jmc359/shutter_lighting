from phue import Bridge

PHUE_IP = "192.168.2.111"
lights = [2,3]
		
b = Bridge(PHUE_IP)
b.get_api()
for light in lights:
	if not b.get_light(light, 'on'):
		b.set_light(light,'on', True)