#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool

"""
Node for sending signal to capture pose
"""
class SendSignalNode:
	def __init__(self):
		rospy.init_node("send_signal", anonymous=True)
		self.pub = rospy.Publisher("/capture", Bool, queue_size=2)
		rospy.sleep(3)
		rospy.loginfo("Press enter to capture pose")

		# Wait for ENTER to send signal
		while not rospy.is_shutdown():
			text = raw_input("")
			if text == "":
				msg = Bool()
				msg.data = True
				self.pub.publish(msg)
				rospy.loginfo("Pose captured!")

if __name__ == "__main__":
	try:
		SendSignalNode()
	except rospy.ROSInterruptException:
		pass