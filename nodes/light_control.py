#!/usr/bin/env python

import rospy
from phue import Bridge
from shutter_lighting.msg import PhueLight


"""
Node for actually changing brightness of Philips bulbs
"""
class LightControlNode:
	def __init__(self):
		# Initialize Phillips bulbs parameters
		rospy.init_node("change_lights", anonymous=True)
		self.PHUE_IP = "192.168.2.111"
		self.lights = [2,3]
		
		# Try communicating with bridge over Internet connection
		try:
			self.b = Bridge(self.PHUE_IP)
			self.b.get_api()
			for light in self.lights:
				if not self.b.get_light(light, 'on'):
					self.b.set_light(light, 'on', True)
		except Exception as e:
			rospy.logerr(e)
			exit(1)

		rospy.on_shutdown(self.hook)
		rospy.Subscriber("/change_lights", PhueLight, self.light_cb, queue_size=2)

	def light_cb(self, msg):
		# Set lights according to message command
		self.b.set_light(1, "bri", msg.b1)
		self.b.set_light(2, "bri", msg.b2)
		self.b.set_light(3, "bri", msg.b3)
		self.b.set_light(4, "bri", msg.b4)

	def hook(self):
		# Turn off lights on shutdown
		for light in self.lights:
			self.b.set_light(light, 'on', False)


if __name__ == "__main__":
	try:
		LightControlNode()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass