#!/usr/bin/env python 

import cv2
import os
import rospkg
import rospy
import numpy as np
from skimage.measure import shannon_entropy
from cv_bridge import CvBridge, CvBridgeError
from shutter_lighting.msg import BoundingBox, BoxArray, PhueLight
from sensor_msgs.msg import JointState, Image, CameraInfo
from std_msgs.msg import Bool

"""
Collects data for RL
"""
class DataNode:
    def __init__(self):
        # Start node
        rospy.init_node("data_collection")
        rospy.on_shutdown(self.hook)

        # Get custom file name (required so there's no accidental overriding)
        try:
            filename = rospy.search_param('datafile')
            self.filename = rospy.get_param(filename)
        except Exception as e:
            rospy.logerr(e)
            exit(1)
        self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "data/{}".format(self.filename))

        # Set relevant attributes
        self.light = None
        self.stable = False
        self.bridge = CvBridge()
        self.data = []

        # Initialize subscribers
        rospy.Subscriber("/face_detect", BoxArray, self.face_cb, queue_size=5)
        rospy.Subscriber("/change_lights", PhueLight, self.light_cb, queue_size=5)
        rospy.Subscriber("/joint_states", JointState, self.joint_cb, queue_size=5)
        rospy.Subscriber("/camera/color/image_raw", Image, self.image_cb, queue_size=5)
        rospy.Subscriber("/camera/depth/image_rect_raw", Image, self.depth_cb, queue_size=5)
        rospy.Subscriber("/camera/depth/camera_info", CameraInfo, self.info_cb, queue_size=5)
        rospy.Subscriber("/stable", Bool, self.stable_cb, queue_size=5)

        # Spin
        rospy.spin()

    def info_cb(self, msg):
        # Camera info callback
        self.K = msg.K 

    def stable_cb(self, msg):
        # Light stability callback
        self.stable = msg.data

    def face_cb(self, msg):
        # Collect data while not transitioning and can see at least one face
        if self.light is not None and self.stable and msg.boxes > 0:
            
            # Extract Shutter joint positions
            both = zip(self.joint.name, self.joint.position)
            both.sort(key=lambda x: x[0])
            joint_positions = [x[1] for x in both]

            # Extract all faces
            for face in msg.boxes:
               
                # Extract image pixel (x,y) pose
                pixel_x = (face.x + face.w) / 2
                pixel_y = (face.y + face.h) / 2

                # Extract XYZ world pose
                z = np.mean(self.depth[face.y:face.y+face.h, face.x:face.x+face.w]) / 1000.0
                x = ((pixel_x - self.K[2]) * z) / self.K[0]
                y = ((pixel_y - self.K[5]) * z) / self.K[4]

                # Determine reward
                # reward = face.entropy + face.local_brightness + face.color

                # Append each face to dataset
                self.data.append([float(self.light.b2), float(self.light.b3), joint_positions[0], joint_positions[1], joint_positions[2], joint_positions[3], 
                                  x, y, z, cv2.resize(self.image[face.y:face.y+face.h, face.x:face.x+face.w], (64, 64)), face.entropy, face.local_brightness, 
                                  face.global_brightness, face.color])


    def depth_cb(self, msg):
        # Depth image callback
        try:
            self.depth = self.bridge.imgmsg_to_cv2(msg, msg.encoding)
        except CvBridgeError as e:
            print(e)

    def light_cb(self, msg):
        # Light transition command callback
    	self.light = msg

    def joint_cb(self, msg):
        # Shutter joint state callback
    	self.joint = msg

    def image_cb(self, msg):
        # Color image callback
    	try:
            self.image = self.bridge.imgmsg_to_cv2(msg, msg.encoding)
        except CvBridgeError as e:
            print(e)

    def hook(self):
        # Hook to cleanup after ROS shutdown
    	self.data = np.array(self.data)
    	np.save(self.filepath, self.data)

if __name__ == "__main__":
    try:
        DataNode()
    except rospy.ROSInterruptException:
        pass

