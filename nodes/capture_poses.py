#!/usr/bin/env python

import os
import yaml
import rospy
import rospkg
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool

"""
Node for automatically capturing poses from Shutter given boolean signal
"""
class CapturePosesNode:
	def __init__(self):
		# Initialize ROS and output file name
		rospy.init_node("capture_poses")
		param_name = rospy.search_param("outputname")
		filename = rospy.get_param(param_name)
		self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "config/{}".format(filename))
		self.capture = False

		rospy.Subscriber("/joint_states", JointState, self.joint_cb, queue_size=2)
		rospy.Subscriber("/capture", Bool, self.bool_cb, queue_size=2)
		rospy.loginfo("Capture Poses node started")
		rospy.spin()

	def bool_cb(self, msg):
		self.capture = msg.data

	def joint_cb(self, msg):
		# Check if signal msg received for capturing pose
		if self.capture:
			positions = dict(zip(msg.name, msg.position))
			data = None
			# Open file if it already exists
			try:
				with open(self.filepath, "r") as f:
					data = yaml.load(f)
			except:
				pass
			# Write as new file if empty or update if already populated
			with open(self.filepath, "w") as f:
				if data:
					data.update({"pose{}".format(len(data.keys())): positions})
					yaml.dump(data, f)
				else:
					yaml.dump({"pose0": positions}, f)

			rospy.loginfo("Captured pose: {}".format(positions))
			self.capture = False

if __name__ == "__main__":
	try:
		CapturePosesNode()
	except rospy.ROSInterruptException:
		pass
