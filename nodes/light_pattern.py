#!/usr/bin/env python

import rospy
from shutter_lighting.msg import PhueLight

"""
Example node for sending brightness to Philips bulbs
"""
class LightPatternNode:
	def __init__(self):
		rospy.init_node("light_pattern", anonymous=True)
		self.pub = rospy.Publisher("/change_lights", PhueLight, queue_size=2)

		while not rospy.is_shutdown():
			for i in range(0, 240, 30):
				p = PhueLight()
				p.b1 = 0
				p.b2 = i
				p.b3 = i
				p.b4 = 0
				self.pub.publish(p)
				rospy.sleep(3)

if __name__ == "__main__":
	try:
		LightPatternNode()
	except rospy.ROSInterruptException:
		pass
