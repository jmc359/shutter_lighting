#!/usr/bin/env python

import os
import yaml
import rospkg
import rospy
from std_msgs.msg import Float64
from shutter_lighting.msg import PhueLight

"""
Node for automatically commanding pose + light combinations from yaml file and loops
"""
class SamePoseNode:
	def __init__(self):
		rospy.init_node("same_poses")
		self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "config/poses.yaml")
		with open(self.filepath, "r") as f:
			self.poses = yaml.load(f)
		self.joint_1 = rospy.Publisher("joint_1/command", Float64, queue_size=1)
		self.joint_2 = rospy.Publisher("joint_2/command", Float64, queue_size=1)
		self.joint_3 = rospy.Publisher("joint_3/command", Float64, queue_size=1)
		self.joint_4 = rospy.Publisher("joint_4/command", Float64, queue_size=1)
		self.bright = rospy.Publisher("/change_lights", PhueLight, queue_size=1)
		rospy.sleep(3)

	def loop(self):
		# Go through light combos
		for bright1 in range(0, 240, 30):
			for bright2 in range(0, 240, 30):
				p = PhueLight()
				p.b1 = 0
				p.b2 = bright1
				p.b3 = bright2
				p.b4 = 0
				self.bright.publish(p)
				rospy.sleep(1)

if __name__ == "__main__":
	try:
		s = SamePoseNode()
		s.loop()
	except rospy.ROSInterruptException:
		pass
