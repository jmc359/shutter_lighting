#!/usr/bin/env python 

import cv2
import os
import rospkg
import rospy
from skimage.measure import shannon_entropy
from cv_bridge import CvBridge, CvBridgeError
from shutter_lighting.msg import BoundingBox, BoxArray
from shutter_lighting.IQ.no_learning.quality_metrics import PictureEvaluator
from sensor_msgs.msg import Image


"""
Node that publishes detected face bounding box info from camera stream to /face_detect.
Detection uses HaarCascades frontalface default by cv2.
"""
class FaceDetectorNode:
    def __init__(self):
        # Initialize ROS and face detector
        rospy.init_node("face_detect")
        self.bridge = CvBridge()
        self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "config/haarcascades/")
        cascPath = os.path.join(self.filepath, "haarcascade_frontalface_default.xml")
        self.faceCascade = cv2.CascadeClassifier(cascPath)
        self.face_pub = rospy.Publisher("face_detect", BoxArray, queue_size=1)
        self.img_sub = rospy.Subscriber("/camera/color/image_raw", Image, self.detect, queue_size=5)
        rospy.spin()

    def detect(self, img):
        # Convert image to CV
        try:
            cv_image = self.bridge.imgmsg_to_cv2(img, "mono8")
            color_image = self.bridge.imgmsg_to_cv2(img, img.encoding)
        except CvBridgeError as e:
            print(e)

        msg = BoxArray()

        # produce header info
        msg.header = img.header

        # detect faces
        faces = self.faceCascade.detectMultiScale(
            cv_image,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(100,100),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        # Populate ROS BoxArray message
        for (x, y, w, h) in faces:
            face = BoundingBox()
            face.x = x
            face.y = y
            face.w = w
            face.h = h
            full_evaluator = PictureEvaluator(color_image)
            face_evaluator = PictureEvaluator(color_image[y:y+h,x:x+w])
            face.color = face_evaluator.get_color_contrast() / 100.0
            face.local_brightness = abs(1.0 / face_evaluator.get_brightness_contrast() * 10)
            face.global_brightness = abs(full_evaluator.get_brightness_contrast() / 10.0)
            face.entropy = shannon_entropy(cv_image[y:y+h, x:x+w])
            msg.boxes.append(face)


        # Publish
        self.face_pub.publish(msg)

if __name__ == "__main__":
    try:
        FaceDetectorNode()
    except rospy.ROSInterruptException:
        pass