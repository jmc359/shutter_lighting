#!/usr/bin/env python

from keras.models import load_model
from keras.optimizers import Adam
import cv2
import os
import rospkg
import rospy
import numpy as np
from threading import Timer
from shutter_lighting.msg import PhueLight
from skimage.measure import shannon_entropy
from cv_bridge import CvBridge, CvBridgeError
from shutter_lighting.msg import BoundingBox, BoxArray, PhueLight
from sensor_msgs.msg import JointState, Image, CameraInfo
from std_msgs.msg import Float64, Bool
import yaml
import tensorflow as tf
from shutter_lighting.nn.ddpg import ActorNetwork

"""
Collects data for RL
"""
class DecisionNode:
    def __init__(self):

        # Initialize ROS
        self.package_path = rospkg.RosPack().get_path("shutter_lighting")
        rospy.init_node("decision")

        # Get model path
        try:
            modelh5 = rospy.search_param('modelh5')
            self.modelh5 = rospy.get_param(modelh5)
            posefile = rospy.search_param('posefile')
            self.posefile = rospy.get_param(posefile)
            posename = rospy.search_param('posename')
            self.posename = rospy.get_param(posename)
            bl = rospy.search_param('bl')
            self.bl = rospy.get_param(bl)
            br = rospy.search_param('br')
            self.br = rospy.get_param(br)
        except Exception as e:
            rospy.logerr(e)
            exit(1)

        self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "config/{}".format(self.posefile))
        with open(self.filepath, "r") as f:
            self.poses = yaml.load(f)

        # Set relevant params
        self.light = None
        self.compute = True
        self.data = []
        self.bridge = CvBridge()
        self.pub = rospy.Publisher("/change_lights", PhueLight, queue_size=2)
        rospy.sleep(2)

        # Initialize lights
        msg = PhueLight()
        msg.b2 = self.br
        msg.b3 = self.bl
        self.pub.publish(msg)

        # Initialize publishers
        self.joint_1 = rospy.Publisher("joint_1/command", Float64, queue_size=1)
        self.joint_2 = rospy.Publisher("joint_2/command", Float64, queue_size=1)
        self.joint_3 = rospy.Publisher("joint_3/command", Float64, queue_size=1)
        self.joint_4 = rospy.Publisher("joint_4/command", Float64, queue_size=1)
        rospy.sleep(2)

        # Set pose
        for key, value in self.poses[self.posename].items():
            msg = Float64()
            msg.data = value
            getattr(self, key).publish(msg)
        rospy.sleep(3)

        # Initialize subscribers and spin
        ddpg = rospy.search_param('use_ddpg')
        use_ddpg = rospy.get_param(ddpg, default=False)
        if use_ddpg:
            face_cb = self.face_cb_ddpg
            self.weights = os.path.join(self.package_path, "weights/actor/cp.ckpt")
        else:
            face_cb = self.face_cb_dqn

        rospy.Subscriber("/face_detect", BoxArray, face_cb, queue_size=5)
        rospy.Subscriber("/change_lights", PhueLight, self.light_cb, queue_size=5)
        rospy.Subscriber("/joint_states", JointState, self.joint_cb, queue_size=5)
        rospy.Subscriber("/camera/color/image_raw", Image, self.image_cb, queue_size=5)
        rospy.Subscriber("/camera/depth/image_rect_raw", Image, self.depth_cb, queue_size=5)
        rospy.Subscriber("/camera/depth/camera_info", CameraInfo, self.info_cb, queue_size=5)
        rospy.spin()

    def reset(self):
        # Timer function resetting whether another action should be taken
        self.compute = True

    def info_cb(self, msg):
        # Camera info callback
        self.K = msg.K

    def depth_cb(self, msg):
        # Depth image callback
        try:
            self.depth = self.bridge.imgmsg_to_cv2(msg, msg.encoding)
        except CvBridgeError as e:
            print(e)

    def face_cb_ddpg(self, msg):
        # Take an action when flag is true and there is at least one face
        if self.compute and len(msg.boxes) > 0:
            self.compute = False
            both = zip(self.joint.name, self.joint.position)
            both.sort(key=lambda x: x[0])
            joint_positions = [x[1] for x in both]

            # Load model
            with tf.Session() as sess:
                tf.global_variables_initializer()

                actor = ActorNetwork(sess, action_dim=2, state_dim=7, action_bound=240,
                            learning_rate=0, batch_size=8)
                actor.model.load_weights(self.weights)

                # Predict best action using model
                l1_actions = []
                l2_actions = []
                for face in msg.boxes:
                    # Extract image pixel (x,y) pose
                    pixel_x = (face.x + face.w) / 2
                    pixel_y = (face.y + face.h) / 2

                    # Extract XYZ world pose
                    z = np.mean(self.depth[face.y:face.y+face.h, face.x:face.x+face.w]) / 1000.0
                    x = ((pixel_x - self.K[2]) * z) / self.K[0]
                    y = ((pixel_y - self.K[5]) * z) / self.K[4]

                    params = np.array([[joint_positions[0], joint_positions[1], joint_positions[2], joint_positions[3], x, y, z]], dtype=object)
                    image = np.array([cv2.resize(self.image[face.y:face.y+face.h, face.x:face.x+face.w], (64, 64)) / 255.0])

                    a_out = actor.predict(params, image)
                    l1_actions.append(round(a_out[0, 0]))
                    l2_actions.append(round(a_out[0, 1]))

            # best actions are average over output brightnesses for each light
            best1 = np.average(l1_actions)
            best2 = np.average(l2_actions)

            # Publish best
            msg = PhueLight()
            msg.b2 = int(best1)
            msg.b3 = int(best2)
            self.pub.publish(msg)
            rospy.logwarn("DDPG prediction published: {} {}".format(best1, best2))


    def face_cb_dqn(self, msg):
        # Take an action when flag is true and there is at least one face
        if self.compute and len(msg.boxes) > 0:
            self.compute = False

            # Load model
            model = load_model(os.path.join(self.package_path, "weights/{}".format(self.modelh5)))
            opt = Adam(lr=1e-4, decay=1e-4 / 200)
            model.compile(loss="mean_absolute_percentage_error", optimizer=opt)
            both = zip(self.joint.name, self.joint.position)
            both.sort(key=lambda x: x[0])
            joint_positions = [x[1] for x in both]

            # Initialize maximization parameters
            best1 = -1
            best2 = -1
            qmax = 0

            # Calculate instantaneous reward over future states/actions
            for face in msg.boxes:
                # Extract image pixel (x,y) pose
                pixel_x = (face.x + face.w) / 2
                pixel_y = (face.y + face.h) / 2

                # Extract XYZ world pose
                z = np.mean(self.depth[face.y:face.y+face.h, face.x:face.x+face.w]) / 1000.0
                x = ((pixel_x - self.K[2]) * z) / self.K[0]
                y = ((pixel_y - self.K[5]) * z) / self.K[4]

                for bright1 in range(0, 270, 30):
                    for bright2 in range(0, 270, 30):
                        params = np.array([[float(bright1), float(bright2), joint_positions[0], joint_positions[1], joint_positions[2], joint_positions[3], x, y, z]], dtype=object)
                        image = np.array([cv2.resize(self.image[face.y:face.y+face.h, face.x:face.x+face.w], (64, 64)) / 255.0])
                        input_data = [params, image]
                        q = model.predict(input_data)
                        if q[0][0] > qmax:
                            qmax = q[0][0]
                            best1 = bright1
                            best2 = bright2
                        rospy.logwarn("q: {} L: {} R: {} ".format(q[0][0], bright2, bright1))
            try:
                with open(os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "results.yaml"), "r") as f:
                    data = yaml.load(f)
            except:
                pass

            # Write as new file if empty or update if already populated
            with open(os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "results.yaml"), "w") as f:
                if data:
                    # data[self.posename].append([self.bl, self.br, qmax, face.entropy, face.local_brightness, face.entropy + face.local_brightness])
                    data[self.posename].update({"{}_{}".format(self.bl, self.br): [float(qmax), float(face.entropy),
                                                 float(face.local_brightness), float(face.entropy + face.local_brightness)]})
                    yaml.dump(data, f)
                rospy.logwarn("Reward: qmax: {} e: {} lb: {} sum: {}".format(qmax, face.entropy, face.local_brightness, face.entropy + face.local_brightness))

            # Publish best
            msg = PhueLight()
            msg.b2 = best1
            msg.b3 = best2
            self.pub.publish(msg)
            rospy.logwarn("DRN predicition published: {} {}".format(best2, best1))


    def light_cb(self, msg):
        # Get light command
    	self.light = msg

    def joint_cb(self, msg):
        # Get joint states
    	self.joint = msg

    def image_cb(self, msg):
        # Get color image feed
    	try:
            self.image = self.bridge.imgmsg_to_cv2(msg, msg.encoding)
        except CvBridgeError as e:
            print(e)

if __name__ == "__main__":
    try:
        DecisionNode()
    except rospy.ROSInterruptException:
        pass
