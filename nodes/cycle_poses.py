#!/usr/bin/env python

import os
import yaml
import rospkg
import rospy
from std_msgs.msg import Float64, Bool
from shutter_lighting.msg import PhueLight

"""
Node for automatically commanding pose + light combinations from yaml file and loops
"""
class CyclePosesNode:
	def __init__(self):
		rospy.init_node("cycle_poses")
		# Get pose config file name and load 
		try:
			filename = rospy.search_param('posefile')
			self.filename = rospy.get_param(filename)
		except Exception as e:
			rospy.logerr(e)
			exit(1)
		self.filepath = os.path.join(rospkg.RosPack().get_path("shutter_lighting"), "config/{}".format(self.filename))
		with open(self.filepath, "r") as f:
			self.poses = yaml.load(f)

		# Initialize subscribers and spin
		self.joint_1 = rospy.Publisher("joint_1/command", Float64, queue_size=1)
		self.joint_2 = rospy.Publisher("joint_2/command", Float64, queue_size=1)
		self.joint_3 = rospy.Publisher("joint_3/command", Float64, queue_size=1)
		self.joint_4 = rospy.Publisher("joint_4/command", Float64, queue_size=1)
		self.bright = rospy.Publisher("/change_lights", PhueLight, queue_size=1)
		self.stable = rospy.Publisher("/stable", Bool, queue_size=1)
		rospy.sleep(3)

	def loop(self):
		# Loop over all poses in file
		for i in range(len(self.poses)):
			pose_name = "pose{}".format(i)

			# Set pose
			for key, value in self.poses[pose_name].items():
				msg = Float64()	
				msg.data = value
				getattr(self, key).publish(msg)
			rospy.sleep(3)

			# Go through all light combos 
			for bright1 in range(0, 240, 30):
				for bright2 in range(0, 240, 30):
					p = PhueLight()
					p.b1 = 0
					p.b2 = bright1
					p.b3 = bright2
					p.b4 = 0
					self.bright.publish(p)

					rospy.sleep(0.7)
					stable = Bool()
					stable.data = True
					self.stable.publish(stable)

					rospy.sleep(1)
					stable = Bool()
					stable.data = False
					self.stable.publish(stable)


if __name__ == "__main__":
	try:
		c = CyclePosesNode()
		c.loop()
	except rospy.ROSInterruptException:
		pass
