#!/usr/bin/env python3

import numpy as np
import cv2
import re
#import imquality.brisque as brisque
from PIL import Image, ImageEnhance
import matplotlib.pyplot as plt 
import math

#import calculations

class PictureEvaluator:
	def __init__(self, image, mean_laplacian_good=np.zeros((480,640)), mean_laplacian_bad=np.zeros((480,640))):
		self.cv_image = image
		if self.cv_image.shape[0] != 480 or self.cv_image.shape[1] != 640:
			self.cv_image = cv2.resize(self.cv_image, (640,480), interpolation=cv2.INTER_AREA)
		self.mean_laplacian_good = mean_laplacian_good
		self.mean_laplacian_bad = mean_laplacian_bad

	def revert_to_old_image(self):
		self.filename = self.original_file
		# self.image = Image.open(self.filename)
		self.cv_image = image

	def show(self):
		# self.image.show()
		cv2.imshow('pic', self.cv_image)
		cv2.waitKey(0)


	def get_blur_factor(self):
		theta = 5
		#grayscale = self.image.convert('L')
		grayscale = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)
		fourier = np.fft.fft2(grayscale)
		C = np.where(fourier > theta, fourier, 0)
		quality = np.linalg.norm(C) / np.linalg.norm(fourier)
		sigma = 1/quality
		return sigma

	def get_clutter_measure(self):
		img_laplacian = calculations.get_normalized_laplacian(self.cv_image)
		diff_bad = np.sum(abs(img_laplacian - self.mean_laplacian_bad))
		diff_good = np.sum(abs(img_laplacian - self.mean_laplacian_good))
		return diff_bad - diff_good

	def get_color_contrast(self):
		histB = cv2.calcHist([self.cv_image], [0], None, [256], [0,255])
		histG = cv2.calcHist([self.cv_image], [1], None, [256], [0,255])
		histR = cv2.calcHist([self.cv_image], [2], None, [256], [0,255])
		hist_combined = histB + histG + histR
		hist_normalized = hist_combined / np.linalg.norm(hist_combined)  # normalize to unit length
		# plt.plot(histB/np.linalg.norm(histB), color='b')
		# plt.plot(histG/np.linalg.norm(histG), color='g')
		# plt.plot(histR/np.linalg.norm(histR), color='r')
		# plt.plot(hist_normalized, color='y')
		# plt.show()
		lower_bound = 0
		upper_bound = 256
		accumulated_mass = 0
		for i in range(len(hist_normalized)):
			accumulated_mass += hist_normalized[i][0] ** 2
			# print('accumulating: {}'.format(math.sqrt(accumulated_mass)))
			if math.sqrt(accumulated_mass) > 0.01:
				lower_bound = i
				break
		accumulated_mass = 1
		for i in reversed(range(len(hist_normalized))):
			accumulated_mass -= hist_normalized[i][0] ** 2
			# print('accumulating: {}'.format(accumulated_mass))
			if accumulated_mass < 0 or math.sqrt(accumulated_mass) < 0.99:
				upper_bound = i
				break
		# print('lower_bound: {}'.format(lower_bound))
		# print('upper_bound: {}'.format(upper_bound))
		middle98_width = upper_bound - lower_bound
		return middle98_width

	def get_brightness_contrast(self):
		hsv_image = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2HSV)
		brightness_channel = hsv_image[:,:,2]
		width, height = np.shape(brightness_channel)
		avg_brightness = np.sum(brightness_channel) / (width*height)
		return avg_brightness - 127.5  # deviation from 50% gray

	def get_hue_count(self):
		alpha = 0.05
		num_bins = 20
		hsv_image = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2HSV)
		lower_bound = np.array([0,51,38])
		upper_bound = np.array([180,255,243])
		mask = cv2.inRange(hsv_image, lower_bound, upper_bound) / 255
		filtered_hues = hsv_image[:,:,0] * mask
		hue_hist, bins = np.histogram(filtered_hues, num_bins, [1,180])
		max_value = max(hue_hist)
		N = np.sum(hue_hist > alpha*max_value)
		#print(np.extract(hue_hist > alpha*max_value, bins))
		return num_bins - N