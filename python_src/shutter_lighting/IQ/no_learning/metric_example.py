from quality_metrics import PictureEvaluator
import argparse

parser = argparse.ArgumentParser(description='Evaluate image quality metrics')
parser.add_argument('images', metavar='IMG', type=str, nargs='+',
                     help='image file paths')
args = parser.parse_args()


for image in args.images:
	print(image)
	evaluator = PictureEvaluator(image)
	# evaluator.show()
	print("blur factor: ", evaluator.get_blur_factor(), " (smaller is better)")
	# print(evaluator.get_clutter_measure())
	print("color contrast: ", evaluator.get_color_contrast())
	print("brightness contrast: ", evaluator.get_brightness_contrast(), " (deviation from 50% gray)")
	print("hue count: ", evaluator.get_hue_count(), " (want small hue count)")
	print("")