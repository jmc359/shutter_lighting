#!/usr/bin/env python

import cv2
import os
import rospkg
import rospy
from skimage.measure import shannon_entropy
from cv_bridge import CvBridge, CvBridgeError
from shutter_lighting.msg import BoundingBox, BoxArray
from sensor_msgs.msg import Image
import numpy as np
import math as m
import sys
from scipy.special import gamma as tgamma
from brisquequality import brisque

"""
Node that publishes detected face bounding box info from camera stream and reward to /face_detect.
Detection uses HaarCascades frontalface default by cv2.
"""
class RewardNode:
    def __init__(self):
        rospy.init_node("reward")

        self.bridge = CvBridge()
        self.package_path = rospkg.RosPack().get_path("shutter_lighting")
        self.filepath = os.path.join(self.package_path, "config/haarcascades/")
        cascPath = os.path.join(self.filepath, "haarcascade_frontalface_default.xml")
        self.faceCascade = cv2.CascadeClassifier(cascPath)

        self.face_pub = rospy.Publisher("face_detect", BoxArray, queue_size=1)

        self.img_sub = rospy.Subscriber("/camera/color/image_raw", Image, self.detect, queue_size=5)
        rospy.spin()

    def detect(self, img):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(img, "mono8")
        except CvBridgeError as e:
            print(e)

        msg = BoxArray()

        # produce header info
        msg.header = img.header

        # detect faces
        faces = self.faceCascade.detectMultiScale(
            cv_image,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(100,100),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        for (x, y, w, h) in faces:
            cropped = cv_image[y:y+h, x:x+w]
            face = BoundingBox()
            face.x = x
            face.y = y
            face.w = w
            face.h = h
            face.brisque = brisque(os.path.join(self.package_path, "brisque/libsvm/python/allmodel"), cropped)
            face.entropy = shannon_entropy(cropped)
            msg.boxes.append(face)

        # publish
        self.face_pub.publish(msg)

if __name__ == "__main__":
    try:
        RewardNode()
    except rospy.ROSInterruptException:
        pass