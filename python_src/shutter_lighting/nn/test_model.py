import numpy as np
from keras.models import load_model
import matplotlib.pyplot as plt
import rospkg
import os
from mpl_toolkits import mplot3d
from ddpg import CriticNetwork, ActorNetwork
import tensorflow as tf

def gen_graph(x, y, z):
    git = plt.figure()
    ax = plt.axes(projection="3d")

    ax.set_xlabel("L1 brightness")
    ax.set_ylabel("L2 brightness")
    ax.set_zlabel("Predicted reward")

    ax.scatter3D(x, y, z)

    plt.show()

def eval_dqn(pkg_path):
    path = os.path.join(pkg_path, "weights/entlocal.h5")
    model = load_model(path)

    datapath = os.path.join(pkg_path, "data/augmented.npy")
    data = np.load(datapath, allow_pickle=True)

    inputs = data[:, 2:9]
    images = data[:, -5] / 255.0
    images = np.array([im for im in images])

    # example 1: image and pose 0
    i = inputs[0, :]
    img = images[300, :, :, :]
    a = []
    for l1 in range(0, 240, 30):
        for l2 in range(0, 240, 30):
            a.append([l1, l2] +list(i))

    r = model.predict([a, np.array([img] * len(a))])

    z_points = r
    x_points = [x[0] for x in a]
    y_points = [y[1] for y in a]

    gen_graph(x_points, y_points, z_points)

    # example 2: image and pose 300
    i = inputs[0, :]
    img = images[300, :, :, :]
    a = []
    for l1 in range(0, 240+1, 30):
        for l2 in range(0, 240+1, 30):
            a.append([l1, l2] + list(i))

    r = model.predict([a, np.array([img] * len(a))])

    z_points = r
    x_points = [x[0] for x in a]
    y_points = [y[1] for y in a]

    gen_graph(x_points, y_points, z_points)

def eval_ddpg_critic(pkg_path):
    critic = CriticNetwork(7, 2, learning_rate=1e-3)
    weights = os.path.join(pkg_path, "weights/critic/cp_no_global_brightness.ckpt")

    critic.model.load_weights(weights)

    data = np.load(os.path.join(pkg_path, "data/augmented_test.npy"), allow_pickle=True)
    actions = data[:, 0:2]
    inputs = data[:, 2:9]
    images = data[:, -5] / 255.0
    images = np.array([im for im in images])
    rewards = data[:, -4] + data[:, -3] + data[:, -1]
    #
    acc = critic.model.evaluate([inputs, images, actions], rewards, verbose=2)
    print("Restored model accuracy: {:5.2f}%".format(acc))

    # good example: compare 0 and 300
    # i = inputs[0, :]
    # img = images[0, :, :, :]
    # a = []
    # for l1 in range(0, 240+1, 5):
    #     for l2 in range(0, 240+1, 5):
    #         a.append([l1, l2])
    #
    # r = critic.model.predict([np.array([i] * len(a)), np.array([img] * len(a)), np.array(a)])
    #
    # z_points = r
    # x_points = [x for x, _ in a]
    # y_points = [y for _, y in a]
    #
    # gen_graph(x_points, y_points, z_points)

    # good example: compare 0 and 300
    i = inputs[200, :]
    img = images[200, :, :, :]
    a = []
    for l1 in range(0, 240+1, 5):
        for l2 in range(0, 240+1, 5):
            a.append([l1, l2])

    r = critic.model.predict([np.array([i] * len(a)), np.array([img] * len(a)), np.array(a)])

    z_points = r
    x_points = [x for x, _ in a]
    y_points = [y for _, y in a]

    gen_graph(x_points, y_points, z_points)

def brute_force_label_actions(pkg_path):
    critic = CriticNetwork(7, 2, learning_rate=1e-3)
    weights = os.path.join(pkg_path, "weights/critic/cp_no_global_brightness.ckpt")

    critic.model.load_weights(weights)

    data = np.load(os.path.join(pkg_path, "data/augmented_test.npy"), allow_pickle=True)
    inputs = data[:, 2:9]
    images = data[:, -5] / 255.0
    images = np.array([im for im in images])

    best_action_per_image = []

    for x in range(inputs.shape[0]):
        i = np.array([inputs[x, :]])
        im = np.array([images[x, :, :, :]])
        ba = [0, 0]
        br = -1000
        for l1 in range(0, 240+1, 10):
            for l2 in range(0, 240+1, 10):
                r = critic.model.predict([i, im, np.array([[l1, l2]])])
                # print("Reward for im", x, "action", l1, l2, r)
                if r > br:
                    br = r
                    ba = [l1, l2]

        best_action_per_image.append([ba, br])

    best = np.array(best_action_per_image)
    np.save("actions.npy", best, allow_pickle=True)

def eval_actor(pkg_path):
    with tf.Session() as sess:
        tf.global_variables_initializer()

        critic = CriticNetwork(7, 2, learning_rate=1e-3)
        weights = os.path.join(pkg_path, "weights/critic/cp_no_global_brightness.ckpt")

        critic.model.load_weights(weights)

        data = np.load(os.path.join(pkg_path, "data/augmented_test.npy"), allow_pickle=True)
        inputs = data[:, 2:9]
        images = data[:, -5] / 255.0
        images = np.array([im for im in images])

        weightpath = os.path.join(pkg_path, "weights/actor/cp_newest.ckpt")

        actor = ActorNetwork(sess, action_dim=2, state_dim=7, action_bound=240, learning_rate=1e-4, batch_size=8)
        actor.model.load_weights(weightpath)

        predictions = actor.predict(inputs, images)
        rewards = critic.model.predict([inputs, images, predictions])

    best = np.load("actions.npy", allow_pickle=True)
    b_rewards = best[:, 1]

    print(best.shape)
    # mape
    sig_under = 0.0
    sig_over = 0.0
    n = b_rewards.shape[0]

    for i in range(b_rewards.shape[0]):
        yt = b_rewards[i][0][0]
        yi = rewards[i][0]

        print(yt, yi)

        if yi > yt:
            sig_over += abs((yt-yi) / yt)
        else:
            sig_under += abs((yt-yi)/yt)

    print(sig_over / n)
    print(sig_under / n)

if __name__ == "__main__":
    pkg = rospkg.RosPack().get_path("shutter_lighting")
    # eval_dqn(pkg)
    # eval_ddpg_critic(pkg)
    # brute_force_label_actions(pkg)
    eval_actor(pkg)
