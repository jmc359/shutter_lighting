#!/usr/bin/env python

import numpy as np
import os
import rospkg
import cv2

if __name__ == "__main__":
	# load images
	path = rospkg.RosPack().get_path("shutter_lighting")
	data = np.load(os.path.join(path, "data/multi.npy"), allow_pickle=True)
	for i in range(0, len(data)):
		cv2.imshow("data", data[i][-2])
		cv2.waitKey(0)