from keras.models import Sequential, load_model
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input, add
from keras.models import Model
# from sklearn.model_selection import train_test_split
from keras.optimizers import Adam
from keras.layers import concatenate
import keras.backend as K
import tensorflow as tf
import numpy as np
import argparse
import locale
import os
import rospkg
import math
from rl_base import create_mlp, create_cnn

import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

class ActorNetwork:
    def __init__(self, sess, state_dim, action_dim, action_bound,
            learning_rate, batch_size):

        self.sess = sess
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.action_bound = action_bound
        self.batch_size = batch_size
        self.lr = learning_rate

        self.inputs, self.images, self.out, self.scaled_out = self.create_actor_network()
        self.network_params = tf.trainable_variables()

        # gradient to be provided by critic network
        self.action_gradient = tf.placeholder(tf.float32, [None, self.action_dim])

        # combine gradients
        self.unnorm_actor_gradients = tf.gradients(
            self.scaled_out, self.network_params, -self.action_gradient)
        self.actor_gradients = list(map(lambda x: tf.math.divide(x, self.batch_size) if x is not None else x,
            self.unnorm_actor_gradients))

        # optimization
        self.opt = tf.train.AdamOptimizer(self.lr).\
            apply_gradients(zip(self.actor_gradients, self.network_params))

        self.num_trainable_vars = len(self.network_params)

    def train(self, inputs, images, a_gradient):
        self.sess.run(self.opt, feed_dict={
            self.inputs: inputs,
            self.images: images,
            self.action_gradient: a_gradient})

    def predict(self, inputs, images):
        return self.sess.run(self.scaled_out, feed_dict={
            self.inputs: inputs,
            self.images: images})

    def get_num_trainable_vars(self):
        return self.num_trainable_vars

    def create_actor_network(self):
        cnn = create_cnn(64, 64, 3, regress=False)

        mlp = Sequential()
        mlp.add(Dense(8, input_dim=self.state_dim, activation="relu"))
        mlp.add(Dense(4, activation="linear"))

        combinedInput = concatenate([mlp.output, cnn.output])
        x = Dense(4, activation="tanh")(combinedInput)
        x = Dense(4, activation="linear")(x)
        out = Dense(2, activation="sigmoid")(x)

        scaled_out = tf.multiply(out, self.action_bound)

        self.model = Model(inputs=[mlp.input, cnn.input], outputs=out)

        return mlp.input, cnn.input, out, scaled_out

class CriticNetwork:
    def __init__(self, state_dim, action_dim, learning_rate, num_actor_vars=0):
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.lr = learning_rate

        # create critic network
        self.inputs, self.images, self.action, self.out, self.model = self.create_critic_network()

        self.network_params = tf.trainable_variables()[num_actor_vars:]

        # network target
        self.predicted_q_value = tf.placeholder(tf.float32, [None, 1])

        # optimization
        self.loss = tf.keras.losses.MeanAbsolutePercentageError()(self.predicted_q_value, self.out)
        self.opt = tf.train.AdamOptimizer(self.lr).minimize(self.loss)

        # Get the gradient of the net w.r.t. the action.
        # For each action in the minibatch (i.e., for each x in xs),
        # this will sum up the gradients of each critic output in the minibatch
        # w.r.t. that action. Each output is independent of all
        # actions except for one.
        self.action_grads = tf.gradients(self.out, self.action)


    def action_gradients(self, sess, inputs, images, actions):
        return sess.run(self.action_grads, feed_dict={
            self.inputs: inputs,
            self.images: images,
            self.action: actions
        })

    def train(self, data, batch_size=50, epochs=50):

        path = rospkg.RosPack().get_path("shutter_lighting")
        cp_path = os.path.join(path, "weights/critic/cp_no_global_brightness.ckpt")
        cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=cp_path,
            save_weights_only=True,
            verbose=1)

    	indices = list(range(data.shape[0]))
    	np.random.shuffle(indices)
    	ntrain = int(data.shape[0] * 0.8)
    	train_indices = indices[:ntrain]
    	test_indices = indices[ntrain:]

    	train = data[train_indices, :]
    	test = data[test_indices, :]

    	train_images = train[:, -5] / 255.0
    	train_images = np.array([im for im in train_images])
    	train_params = train[:, 2:9]
        train_actions = train[:, 0:2]
    	# train_reward = train[:, -3] + train[:, -4] + [:, -2]
        train_reward = train[:, -4] + train[:, -3] + train[:, -1]

    	test_images = test[:, -5] / 255.0
    	test_images = np.array([im for im in test_images])
        test_params = test[:, 2:9]
        test_actions = test[:, 0:2]
    	test_reward = test[:, -4] + test[:, -3] + test[:, -1]

        print("[INFO] Training critic model...")
        # print(train_params.shape)
        # print(test_params.shape)

        self.model.fit([train_params, train_images, train_actions], train_reward,
                    validation_data=([test_params, test_images, test_actions], test_reward),
                    epochs=epochs, batch_size=batch_size, callbacks=[cp_callback])

    def create_critic_network(self):
        inputs = Input(shape=(self.state_dim,))
        actions = Input(shape=(self.action_dim,))

        comb1 = concatenate([inputs, actions])
        x = Dense(self.state_dim + self.action_dim, activation="linear")(comb1)
        x = Dense(8, activation="relu")(x)
        x = Dense(4, activation="linear")(x)

        mlp = Model(inputs=[inputs, actions], outputs=x)

        cnn = create_cnn(64, 64, 3, regress=False)
        images = cnn.input

        comb2 = concatenate([mlp.output, cnn.output])
        x = Dense(4, activation="sigmoid")(comb2)
        # x = Dense(4, activation="linear")(x)
        out = Dense(1, activation="linear")(x)

        model = Model(inputs=[inputs, images, actions], outputs=out)

        opt = Adam(self.lr, decay=5e-4/50)
        model.compile(loss="mean_absolute_percentage_error", optimizer=opt)
        print(model.summary())

        return inputs, images, actions, out, model

def train_actor_handler(sess, actor, critic, data, batch_size, epochs):
    sess.run(tf.global_variables_initializer())
    path = rospkg.RosPack().get_path("shutter_lighting")
    weights = os.path.join(path, "weights/actor/cp_newest.ckpt")
    actor.model.load_weights(weights)

    print("[INFO] Training loaded actor model...")

    for i in xrange(epochs):
        ep_reward = 0.0
        a_outs = []

        for j in range(0, data.shape[0]-batch_size+1, batch_size):
            states=data[j:j+batch_size, 2:9]
            images=data[j:j+batch_size, -5] / 255.0
            images = np.array([im for im in images])
            # actions=data[j:j+batch_size, 0:2]

            # rewards=data[j:j+batch_size, -3] + data[j:j+batch_size, -4]

            a_outs = actor.predict(states, images)
            grads = critic.action_gradients(sess, states, images, a_outs)
            actor.train(states, images, grads[0])


            rewards = critic.model.predict([states, images, a_outs])

            total_rewards = np.sum(rewards)
            if total_rewards==0:
                for i in range(3):
                    print(a_outs[i], rewards[i])
            ep_reward += total_rewards

        print('| Epoch: {:d} | Reward: {:4.3f}'.format(i, float(ep_reward / 100)))
        print(a_outs[:3, :])

    actor.model.save_weights(weights)

def train_actor(batch_size, epochs, lr):
    critic = CriticNetwork(7, 2, learning_rate=1e-3)
    path = rospkg.RosPack().get_path("shutter_lighting")
    weights = os.path.join(path, "weights/critic/cp_no_global_brightness.ckpt")

    critic.model.load_weights(weights)

    data = np.load(os.path.join(path, "data/augmented_test.npy"), allow_pickle=True)

    with tf.Session() as sess:
        actor = ActorNetwork(sess, 7, 2, action_bound=240, learning_rate=lr, batch_size=batch_size)

        train_actor_handler(sess, actor, critic, data, batch_size, epochs)

def train_critic(batch_size, epochs, lr):
    critic = CriticNetwork(7, 2, learning_rate=lr)

    path = rospkg.RosPack().get_path("shutter_lighting")
    data = np.load(os.path.join(path, "data/augmented.npy"), allow_pickle=True)

    indices = list(range(data.shape[0]))
    np.random.shuffle(indices)
    ntrain = int(data.shape[0] * 0.9)
    train_indices = indices[:ntrain]
    test_indices = indices[ntrain:]

    train_data = data[train_indices, :]
    test_data = data[test_indices, :]

    np.save(os.path.join(path, "data/augmented_test.npy"), test_data, allow_pickle=True)

    critic.train(train_data, batch_size=batch_size, epochs=epochs)

def test_critic():
    critic = CriticNetwork(7, 2, learning_rate=1e-3)
    path = rospkg.RosPack().get_path("shutter_lighting")
    weights = os.path.join(path, "weights/critic/cp_no_global_brightness.ckpt")

    critic.model.load_weights(weights)

    data = np.load(os.path.join(path, "data/augmented_test.npy"), allow_pickle=True)
    actions = data[:, 0:2]
    inputs = data[:, 2:9]
    images = data[:, -5] / 255.0
    images = np.array([im for im in images])
    rewards = data[:, -4] + data[:, -3] + data[:, -1]

    acc = critic.model.evaluate([inputs, images, actions], rewards, verbose=2)
    print("Restored model accuracy: {:5.2f}%".format(acc))

    # good example: compare 0 and 300
    i = inputs[300, :]
    img = images[300, :, :, :]
    a = []
    for l1 in range(0, 240, 30):
        for l2 in range(0, 240, 30):
            a.append([l1, l2])

    r = critic.model.predict([np.array([i] * len(a)), np.array([img] * len(a)), np.array(a)])

    git = plt.figure()
    ax = plt.axes(projection="3d")

    z_points = r
    x_points = [x for x, _ in a]
    y_points = [y for _, y in a]

    for act, rew in zip(a, r):
        print(act, rew)

    ax.scatter3D(x_points, y_points, z_points)

    plt.show()

# def test_actor():
#     critic = CriticNetwork(7, 2, learning_rate=1e-3)
#     path = rospkg.RosPack().get_path("shutter_lighting")
#     weights = os.path.join(path, "weights/critic/cp_no_global_brightness.ckpt")
#
#     critic.model.load_weights(weights)
#
#     actor = ActorNetwork(7, 2, learning_rate=1e-3)


if __name__ == "__main__":
    # train_critic(8, 50, 5e-4)
    # test_critic()
    train_actor(8, 15, 1e-2)
    # pass
