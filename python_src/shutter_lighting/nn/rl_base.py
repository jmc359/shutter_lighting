#!/usr/bin/env python

from keras.models import Sequential, load_model
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.models import Model
from sklearn.model_selection import train_test_split
from keras.optimizers import Adam
from keras.layers import concatenate
import tensorflow as tf
import numpy as np
import argparse
import locale
import os
import rospkg

def create_mlp(dim, regress=False):
	# define our MLP network
	model = Sequential()
	model.add(Dense(8, input_dim=dim, activation="relu"))
	model.add(Dense(4, activation="relu"))

	# check to see if the regression node should be added
	if regress:
		model.add(Dense(1, activation="linear"))

	# return our model
	return model

def create_cnn(width, height, depth, filters=(16, 32, 64), regress=False):
	# initialize the input shape and channel dimension, assuming
	# TensorFlow/channels-last ordering
	inputShape = (height, width, depth)
	chanDim = -1

	# define the model input
	inputs = Input(shape=inputShape)

	# loop over the number of filters
	for (i, f) in enumerate(filters):
		# if this is the first CONV layer then set the input
		# appropriately
		if i == 0:
			x = inputs

		# CONV => RELU => BN => POOL
		x = Conv2D(f, (3, 3), padding="same")(x)
		x = Activation("relu")(x)
		x = BatchNormalization(axis=chanDim)(x)
		x = MaxPooling2D(pool_size=(2, 2))(x)

		# flatten the volume, then FC => RELU => BN => DROPOUT
		x = Flatten()(x)
		x = Dense(16)(x)
		x = Activation("relu")(x)
		x = BatchNormalization(axis=chanDim)(x)
		x = Dropout(0.5)(x)

		# apply another FC layer, this one to match the number of nodes
		# coming out of the MLP
		x = Dense(4)(x)
		x = Activation("relu")(x)

		# check to see if the regression node should be added
		if regress:
			x = Dense(1, activation="linear")(x)

		# construct the CNN
		model = Model(inputs, x)

		# return the CNN
		return model


if __name__ == "__main__":
	# load images
	path = rospkg.RosPack().get_path("shutter_lighting")
	data = np.load(os.path.join(path, "data/augmented.npy"), allow_pickle=True)

	# step 1: split into train-validation and test set
	indices = list(range(data.shape[0]))
	np.random.shuffle(indices)
	ntrain = int(data.shape[0] * 0.9)
	train_indices = indices[:ntrain]
	test_indices = indices[ntrain:]

	train_data = data[train_indices, :]
	test  = data[test_indices, :]

	# set aside and process test data
	test_images = test[:, -5] / 255.0
	test_images = np.array([im for im in test_images])
	test_params = test[:, 0:9]
	test_reward = test[:, -4] + test[:, -3]

	# step 2: split train-validation set into train and validation
	indices = list(range(train_data.shape[0]))
	np.random.shuffle(indices)
	ntrain = int(train_data.shape[0] * 0.9)
	train_indices = indices[:ntrain]
	valid_indices = indices[ntrain:]

	train = train_data[train_indices, :]
	valid = train_data[valid_indices, :]

	train_images = train[:, -5] / 255.0
	train_images = np.array([im for im in train_images])

	# train_images = train_images.reshape(train_images.shape[0], 64, 64, 3)
	train_params = train[:, 0:9]
	train_reward = train[:, -3] + train[:, -4]

	valid_images = valid[:, -5] / 255.0
	valid_images = np.array([im for im in valid_images])

	# valid_images = valid_images.reshape(valid_images.shape[0], 64, 64, 3)
	valid_params = valid[:, 0:9]
	valid_reward = valid[:, -3] + valid[:, -4]

	# create the MLP and CNN models
	mlp = create_mlp(9, regress=False)
	cnn = create_cnn(64, 64, 3, regress=False)

	# create the input to our final set of layers as the *output* of both
	# the MLP and CNN
	combinedInput = concatenate([mlp.output, cnn.output])

	# our final FC layer head will have two dense layers, the final one
	# being our regression head
	x = Dense(4, activation="relu")(combinedInput)
	x = Dense(1, activation="linear")(x)

	# our final model will accept categorical/numerical data on the MLP
	# input and images on the CNN input, outputting a single value (the
	# predicted price of the house)
	model = Model(inputs=[mlp.input, cnn.input], outputs=x)

	opt = Adam(lr=5e-4, decay=5e-4 / 50)
	model.compile(loss="mean_absolute_percentage_error", optimizer=opt)

	# train the model
	print("[INFO] training model...")
	# checkpointCallback = tf.keras.callbacks.ModelCheckpoint(os.path.join(path, "weights/exp1.h5"),
	# 														monitor="val_loss",
	# 														verbose=0,
	# 														save_best_only=True,
	# 														save_weights_only=True,
	# 														mode="auto",
	# 														period=1)
	model.fit(
		[train_params, train_images], train_reward,
		validation_data=([valid_params, valid_images], valid_reward),
		epochs=20, batch_size=6)
	# print model.predict([test_params, test_images])


	err = model.evaluate([test_params, test_images], test_reward)
	print(err)

	model.save(os.path.join(path, "weights/entlocal.h5"))
