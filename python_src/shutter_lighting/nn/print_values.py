#!/usr/bin/env python

from keras.models import Sequential, load_model
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.models import Model
from sklearn.model_selection import train_test_split
from keras.optimizers import Adam
from keras.layers import concatenate
import numpy as np
import argparse
import locale
import os
import rospkg

def create_mlp(dim, regress=False):
	# define our MLP network
	model = Sequential()
	model.add(Dense(8, input_dim=dim, activation="relu"))
	model.add(Dense(4, activation="relu"))

	# check to see if the regression node should be added
	if regress:
		model.add(Dense(1, activation="linear"))

	# return our model
	return model

def create_cnn(width, height, depth, filters=(16, 32, 64), regress=False):
	# initialize the input shape and channel dimension, assuming
	# TensorFlow/channels-last ordering
	inputShape = (height, width, depth)
	chanDim = -1

	# define the model input
	inputs = Input(shape=inputShape)

	# loop over the number of filters
	for (i, f) in enumerate(filters):
		# if this is the first CONV layer then set the input
		# appropriately
		if i == 0:
			x = inputs

		# CONV => RELU => BN => POOL
		x = Conv2D(f, (3, 3), padding="same")(x)
		x = Activation("relu")(x)
		x = BatchNormalization(axis=chanDim)(x)
		x = MaxPooling2D(pool_size=(2, 2))(x)

		# flatten the volume, then FC => RELU => BN => DROPOUT
		x = Flatten()(x)
		x = Dense(16)(x)
		x = Activation("relu")(x)
		x = BatchNormalization(axis=chanDim)(x)
		x = Dropout(0.5)(x)

		# apply another FC layer, this one to match the number of nodes
		# coming out of the MLP
		x = Dense(4)(x)
		x = Activation("relu")(x)

		# check to see if the regression node should be added
		if regress:
			x = Dense(1, activation="linear")(x)

		# construct the CNN
		model = Model(inputs, x)

		# return the CNN
		return model



if __name__ == "__main__":
	path = rospkg.RosPack().get_path("shutter_lighting")
	data = np.load(os.path.join(path, "data/augmented.npy"), allow_pickle=True)
	print(data.shape)
	print(data[0:10])
	exit(0)

	indices = list(range(data.shape[0]))
	np.random.shuffle(indices)
	ntrain = int(data.shape[0] * 0.8)
	train_indices = indices[:ntrain]
	test_indices = indices[ntrain:]

	train = data[train_indices, :]
	test = data[test_indices, :]

	train_images = train[:, -2] / 255.0
	train_images = np.array([im for im in train_images])
	
	# train_images = train_images.reshape(train_images.shape[0], 64, 64, 3)
	train_params = train[:, 0:7]
	train_reward = train[:, -1]

	test_images = test[:, -2] / 255.0
	test_images = np.array([im for im in test_images])
	
	# test_images = test_images.reshape(test_images.shape[0], 64, 64, 3)
	test_params = test[:, 0:7]
	test_reward = test[:, -1]

	print train_params.shape, train_images.shape

	model = load_model(os.path.join(path, "weights/new.h5"))
	# opt = Adam(lr=1e-4, decay=1e-4 / 200)
	# model.compile(loss="mean_absolute_percentage_error", optimizer=opt)
	model.summary()

	print model.predict([np.array([[120.0, 60.0, 0.01227184630308513, -1.581534192310096,
        1.6382914814618648, 0.02761165418194154]], dtype=object), np.array([test_images[0]])])
	print np.array([test_params[0]]).shape, np.array([test_images[0]]).shape
	print [np.array([test_params[0]]), np.zeros((1,64,64,3))]
