# Light Control

## First Time Setup
To control the lights using the Philips API for the first time on your computer, you must obtain the IP of the bridge (likely through using `ifconfig` and seeing your network from admin status in browser). This assumes that your computer and the bridge are somehow connected to the same network, which itself is connected to the Internet. Once all this is done, press the button on the Philips Bridge. Then, if you are able to run the following code without issue, your bulbs are properly connected!

```python
from phue import Bridge

# Create Bridge object
b = Bridge('ip_of_your_bridge')

# If the app is not registered and the button is not pressed, press the button and call connect() (this only needs to be run a single time)
b.connect()

# Get the bridge state (This returns the full dictionary that you can explore)
b.get_api()

# Prints if light 2 is on or not
b.get_light(2, 'on')

# Set brightness of lamp 2 to max
b.set_light(2, 'bri', 254)
```

## AKW 400 Lab Specific Instructions
**In the lab, you must be connected to the `Madame Armoire` network in order for the bulb controls to work.** The username and password are on the router on the desk to the left of the Shutter setup in case you are connecting for the first time.