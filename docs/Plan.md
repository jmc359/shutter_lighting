# Conceptual Plan

We plan on using reinforcement learning (Q-learning) to determine the optimal policy for Shutter to take given an input image and the ability to adjust individual lights' brightness settings.

The model will have the following parameters:
- `S`: The set of states, where any state `s` consists of an image, 3D face information 
(camera can extract depth, and we assume single face), and potentially the pose of the robot.
- `A`: The set of actions, where any action `a` for each light is the tuple 
`[c1, c2, c3]`, where `0 < ci < 1`.
- `R`: The reward function. This would be a combination of image quality metrics to be decided and tested in the near future.

We can use these parameters in conjunction with Q-learning to estimate the optimal policy for Shutter in lighting photos.
