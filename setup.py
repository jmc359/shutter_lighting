# DO NOT RUN DIRECTLY

#!/usr/bine/env python
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup()
d['packages'] = ["shutter_lighting"] #, "shutter_lighting.nn", "shutter_lighting.IQ"]
d['package_dir'] = {"":"python_src"}

setup(**d)